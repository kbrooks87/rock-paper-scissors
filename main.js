/*
1. user makes choice

2. computer makes random choice

3. compare function determines who is winner
*/

let choice = Math.floor(Math.random() * 3)

const decision = document.createElement("div")

const win = document.createTextNode("You Win!!!")

const lose = document.createTextNode("You Lose Try Again")

const tie = document.createTextNode("tied")

let rock = function() {
    if (choice === 1) {
        decision.appendChild(lose)
        document.getElementById("output").appendChild(decision)
    } else if (choice === 2) {
        decision.appendChild(win)
        document.getElementById("output").appendChild(decision)
    } else {
        decision.appendChild(tie)
        document.getElementById("output").appendChild(decision)
    }
}

let paper = function() {
    if (choice === 2) {
        decision.appendChild(lose)
        document.getElementById("output").appendChild(decision)
    } else if (choice === 0) {
        decision.appendChild(win)
        document.getElementById("output").appendChild(decision)
    } else {
        decision.appendChild(tie)
        document.getElementById("output").appendChild(decision)
    }
}

let scissors = function() {
    if (choice === 0) {
        decision.appendChild(lose)
        document.getElementById("output").appendChild(decision)
    } else if (choice === 1) {
        decision.appendChild(win)
        document.getElementById("output").appendChild(decision)
    } else {
        decision.appendChild(tie)
        document.getElementById("output").appendChild(decision)
    }
}

function refresh() {
    window.location.reload();
}